import api from 'services/apiClient'

import { HttpsPostSaleResponse, HttpsPostSalePayload } from './types'

export const postSale =
  async ( payload: HttpsPostSalePayload): Promise<HttpsPostSaleResponse> => {
    const { data } = await api.post<HttpsPostSaleResponse>(
      '/api/sale/',
      payload
    )
    return data
  }

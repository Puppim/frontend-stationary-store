import api from 'services/apiClient'

import { SaleInterfacePayload } from './types'

export const getSaleById =
  async (id: number): Promise<SaleInterfacePayload> => {
    const { data } = await api.get<SaleInterfacePayload>(
      `/api/sale/${id}/`
    )
    return data
  }
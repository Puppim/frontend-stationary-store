export interface SaleInterfacePayload  {
  number_nfe: string
  customers: number
  commission_day: number
  seller: number
  total_amount: string
}

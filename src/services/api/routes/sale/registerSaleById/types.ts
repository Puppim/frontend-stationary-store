export interface HttpsPostSalePayload  {
  number_nfe?: string
  customers?: number
  commission_day?: number
  seller?: number
}

export interface HttpsPostSaleResponse {
  id?: number
  number_nfe?: string
  created_at?: string
  updated_at?: string
  customers?: number
  commission_day?: number
  seller?: number
  total_amount?: string
}

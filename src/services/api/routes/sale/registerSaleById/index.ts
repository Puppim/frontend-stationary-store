import api from 'services/apiClient'

import { HttpsPostSaleResponse, HttpsPostSalePayload } from './types'

export const patchSaleById =
  async (id: number, payload: HttpsPostSalePayload): Promise<HttpsPostSaleResponse> => {
    const { data } = await api.patch<HttpsPostSaleResponse>(
      `/api/sale/${id}/`,
      payload
    )
    return data
  }

export const postSaleById =
  async ( payload: HttpsPostSalePayload): Promise<HttpsPostSaleResponse> => {
    const { data } = await api.post<HttpsPostSaleResponse>(
      '/api/sale/',
      payload
    )
    return data
  }

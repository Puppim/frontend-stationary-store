import api from 'services/apiClient'

import { HttpsPostResponseSale } from './types'

export const getSale =
  async (): Promise<HttpsPostResponseSale> => {
    const { data } = await api.get<HttpsPostResponseSale>(
      '/api/saleall/'
    )
    return data
  }

export interface HttpsPostResponseSale {
  count: number
  next: string|null
  previous: string|null
  results: SaleInterface[]
}

export interface SaleInterface {
  id: number
  number_nfe: string
  created_at: string
  updated_at: string
  customers: Customerinterface
  commission_day: commission_day
  seller: Customerinterface
  total_amount: string
}

export interface Customerinterface {
      id: number
      first_name: string
      last_name: string
      email: string
      telephone: string
}
export interface commission_day {
      id: number
}

import api from 'services/apiClient'

import { HttpsPostCustomersResponse, HttpsPostCustomerPayload } from './types'

export const postCustomers =
  async ( payload: HttpsPostCustomerPayload): Promise<HttpsPostCustomersResponse> => {
    const { data } = await api.post<HttpsPostCustomersResponse>(
      '/api/customers/',
      payload
    )
    return data
  }

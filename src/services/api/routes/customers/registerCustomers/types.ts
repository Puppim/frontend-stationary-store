export interface HttpsPostCustomerPayload  {
  first_name?: string
  last_name?: string
  email?: string
  telephone?: string
}

export interface HttpsPostCustomersResponse {
  id?: number
  first_name?: string
  last_name?: string
  email?: string
  telephone?: string
}

import api from 'services/apiClient'

import { HttpsPostCustomersResponse, HttpsPostCustomerPayload } from './types'

export const patchCustomersById =
  async (id: number, payload: HttpsPostCustomerPayload): Promise<HttpsPostCustomersResponse> => {
    const { data } = await api.patch<HttpsPostCustomersResponse>(
      `/api/customers/${id}/`,
      payload
    )
    return data
  }

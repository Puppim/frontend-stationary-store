export interface HttpsPostResponseCustomers {
  count: number
  next: string|null
  previous: string|null
  results: CustomersInterface[]
}

export interface CustomersInterface {
  id: number
  first_name: string
  last_name: string
  email: string
  telephone: string
}

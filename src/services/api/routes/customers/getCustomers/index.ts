import api from 'services/apiClient'

import { HttpsPostResponseCustomers } from './types'

export const getCustomers =
  async (): Promise<HttpsPostResponseCustomers> => {
    const { data } = await api.get<HttpsPostResponseCustomers>(
      '/api/customers/'
    )
    return data
  }

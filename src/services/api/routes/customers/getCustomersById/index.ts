import api from 'services/apiClient'

import { CustomersInterfacePayload } from './types'

export const getCustomersById =
  async (id: number): Promise<CustomersInterfacePayload> => {
    const { data } = await api.get<CustomersInterfacePayload>(
      `/api/customers/${id}/`
    )
    return data
  }
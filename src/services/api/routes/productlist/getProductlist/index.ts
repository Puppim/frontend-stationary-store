import api from 'services/apiClient'

import { HttpsPostResponseProductlist } from './types'

export const getProductlist =
  async (): Promise<HttpsPostResponseProductlist> => {
    const { data } = await api.get<HttpsPostResponseProductlist>(
      '/api/productlist/'
    )
    return data
  }

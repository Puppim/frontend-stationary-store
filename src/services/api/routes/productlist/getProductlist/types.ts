export interface HttpsPostResponseProductlist {
  count: number
  next: string|null
  previous: string|null
  results: ProductlistInterface[]
}

export interface ProductlistInterface {
  id: number
  first_name: string
  last_name: string
  email: string
  telephone: string
}

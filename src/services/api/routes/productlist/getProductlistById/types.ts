export interface ProductlistInterfacePayload  {
  sale?: number
  product?: number
  quantity?: number
}


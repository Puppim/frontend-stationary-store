import api from 'services/apiClient'

import { ProductlistInterfacePayload } from './types'

export const getCustomersById =
  async (id: number): Promise<ProductlistInterfacePayload> => {
    const { data } = await api.get<ProductlistInterfacePayload>(
      `/api/customers/${id}/`
    )
    return data
  }
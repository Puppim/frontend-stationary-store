export interface HttpsPostProductlistPayload  {
  sale?: number
  product?: number
  quantity?: number
}

export interface HttpsPostProductlistResponse {
  sale?: number
  product?: number
  quantity?: number
}

import api from 'services/apiClient'

import { HttpsPostProductlistResponse, HttpsPostProductlistPayload } from './types'

export const postProductlistById =
  async (payload: HttpsPostProductlistPayload): Promise<HttpsPostProductlistResponse> => {
    const { data } = await api.post<HttpsPostProductlistResponse>(
      'api/Productlist/',
      payload
    )
    return data
  }

import api from 'services/apiClient'

import { HttpsPostProductlistResponse, HttpsPostProductlistPayload } from './types'

export const postProductlist =
  async ( payload: HttpsPostProductlistPayload): Promise<HttpsPostProductlistResponse> => {
    const { data } = await api.post<HttpsPostProductlistResponse>(
      '/api/productlist/',
      payload
    )
    return data
  }

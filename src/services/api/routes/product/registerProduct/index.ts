import api from 'services/apiClient'

import { HttpsPostProductResponse, HttpsPostProductPayload } from './types'

export const postProduct =
  async ( payload: HttpsPostProductPayload): Promise<HttpsPostProductResponse> => {
    const { data } = await api.post<HttpsPostProductResponse>(
      '/api/product/',
      payload
    )
    return data
  }

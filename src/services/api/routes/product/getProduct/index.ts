import api from 'services/apiClient'

import { HttpsPostResponseProduct } from './types'

export const getProduct =
  async (): Promise<HttpsPostResponseProduct> => {
    const { data } = await api.get<HttpsPostResponseProduct>(
      '/api/product/'
    )
    return data
  }

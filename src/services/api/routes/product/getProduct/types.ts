export interface HttpsPostResponseProduct {
  count: number
  next: string|null
  previous: string|null
  results: ProductInterface[]
}

export interface ProductInterface {
  id: number
  description: string
  unitary_value: string
  commission: number
}



export interface ProductInterfacePayload  {
  id?: number
  description?: string
  unitary_value?: string
  commission?: number
}

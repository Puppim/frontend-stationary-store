import api from 'services/apiClient'

import { ProductInterfacePayload } from './types'

export const getProductById =
  async (id: number): Promise<ProductInterfacePayload> => {
    const { data } = await api.get<ProductInterfacePayload>(
      `/api/product/${id}/`
    )
    return data
  }
export interface HttpsPostProductPayload  {
  id?: number
  description?: string
  unitary_value?: string
  commission?: number
}

export interface HttpsPostProductResponse {
  id?: number
  description?: string
  unitary_value?: string
  commission?: number
  created_at?: string
  updated_at?: string
}
import api from 'services/apiClient'

import { HttpsPostProductResponse, HttpsPostProductPayload } from './types'

export const patchProductById =
  async (id: number, payload: HttpsPostProductPayload): Promise<HttpsPostProductResponse> => {
    const { data } = await api.patch<HttpsPostProductResponse>(
      `/api/product/${id}/`,
      payload
    )
    return data
  }

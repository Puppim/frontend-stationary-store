import api from 'services/apiClient'

import { HttpsGetResponseCommissionSeller } from './types'

export const getCommissionSellerById =
  async (id: number): Promise<HttpsGetResponseCommissionSeller> => {
    const { data } = await api.get<HttpsGetResponseCommissionSeller>(
      `/api/commission/?seller=${id}`
    )
    return data
  }
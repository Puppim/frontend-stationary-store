export interface HttpsGetResponseCommissionSeller {
  results: CommissionSellerInterface[]
}

export interface CommissionSellerInterface {
  id: number
  commission_amount: string
  created_at: string
  updated_at: string
  seller: number
  productList: number
}
export interface HttpsPostResponseCommission {
  count: number
  next: string|null
  previous: string|null
  results: CommissionInterface[]
}

export interface CommissionInterface {
  id: number
  first_name: string
  last_name: string
  email: string
  telephone: string
}

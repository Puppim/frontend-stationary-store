import api from 'services/apiClient'

import { HttpsPostResponseCommission } from './types'

export const getCommission =
  async (): Promise<HttpsPostResponseCommission> => {
    const { data } = await api.get<HttpsPostResponseCommission>(
      '/api/commission/'
    )
    return data
  }

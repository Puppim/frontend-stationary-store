export interface HttpsPostCommissionPayload  {
  first_name?: string
  last_name?: string
  email?: string
  telephone?: string
}

export interface HttpsPostCommissionResponse {
  id?: number
  first_name?: string
  last_name?: string
  email?: string
  telephone?: string
}

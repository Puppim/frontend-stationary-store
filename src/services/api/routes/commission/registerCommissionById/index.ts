import api from 'services/apiClient'

import { HttpsPostCommissionResponse, HttpsPostCommissionPayload } from './types'

export const patchCommissionById =
  async (id: number, payload: HttpsPostCommissionPayload): Promise<HttpsPostCommissionResponse> => {
    const { data } = await api.patch<HttpsPostCommissionResponse>(
      `/api/Commission/${id}/`,
      payload
    )
    return data
  }

import api from 'services/apiClient'

import { CommissionInterfacePayload } from './types'

export const getCommissionById =
  async (id: number): Promise<CommissionInterfacePayload> => {
    const { data } = await api.get<CommissionInterfacePayload>(
      `/api/commission/${id}/`
    )
    return data
  }
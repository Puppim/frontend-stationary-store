import api from 'services/apiClient'

import { HttpsPostCommissionResponse, HttpsPostCommissionPayload } from './types'

export const postCommission =
  async ( payload: HttpsPostCommissionPayload): Promise<HttpsPostCommissionResponse> => {
    const { data } = await api.post<HttpsPostCommissionResponse>(
      '/api/commission/',
      payload
    )
    return data
  }

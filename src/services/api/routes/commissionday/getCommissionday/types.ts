export interface HttpsPostResponseCommissionday {
  count: number
  next: string|null
  previous: string|null
  results: CommissiondayInterface[]
}

export interface CommissiondayInterface {
  id: number
  days: number
  max_commission: number
  min_commission: number
  created_at: string
  updated_at: string
}

import api from 'services/apiClient'

import { HttpsPostResponseCommissionday } from './types'

export const getCommissionday =
  async (): Promise<HttpsPostResponseCommissionday> => {
    const { data } = await api.get<HttpsPostResponseCommissionday>(
      '/api/commissionday/'
    )
    return data
  }

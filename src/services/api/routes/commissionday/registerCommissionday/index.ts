import api from 'services/apiClient'

import { HttpsPostCommissiondayResponse, HttpsPostCommissiondayPayload } from './types'

export const postCommissionday =
  async ( payload: HttpsPostCommissiondayPayload): Promise<HttpsPostCommissiondayResponse> => {
    const { data } = await api.post<HttpsPostCommissiondayResponse>(
      '/api/commissionday/',
      payload
    )
    return data
  }

import api from 'services/apiClient'

import { CommissiondayInterfacePayload } from './types'

export const getCommissiondayById =
  async (id: number): Promise<CommissiondayInterfacePayload> => {
    const { data } = await api.get<CommissiondayInterfacePayload>(
      `/api/commissionday/${id}/`
    )
    return data
  }
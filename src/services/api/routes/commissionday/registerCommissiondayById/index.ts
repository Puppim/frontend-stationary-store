import api from 'services/apiClient'

import { HttpsPostCommissiondayResponse, HttpsPostCommissiondayPayload } from './types'

export const patchCommissiondayById =
  async (id: number, payload: HttpsPostCommissiondayPayload): Promise<HttpsPostCommissiondayResponse> => {
    const { data } = await api.patch<HttpsPostCommissiondayResponse>(
      `/api/commissionday/${id}/`,
      payload
    )
    return data
  }

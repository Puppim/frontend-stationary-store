export interface HttpsPostCommissiondayPayload  {
  id?: number
  days?: number
  max_commission?: number
  min_commission?: number
  created_at?: string
  updated_at?: string
}

export interface HttpsPostCommissiondayResponse {
  id?: number
  days?: number
  max_commission?: number
  min_commission?: number
  created_at?: string
  updated_at?: string
}

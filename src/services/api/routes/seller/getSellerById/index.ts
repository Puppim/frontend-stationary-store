import api from 'services/apiClient'

import { SellerInterfacePayload } from './types'

export const getSellerById =
  async (id: number): Promise<SellerInterfacePayload> => {
    const { data } = await api.get<SellerInterfacePayload>(
      `/api/seller/${id}/`
    )
    return data
  }
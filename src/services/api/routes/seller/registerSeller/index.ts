import api from 'services/apiClient'

import { HttpsPostSellerResponse, HttpsPostSellerPayload } from './types'

export const postSeller =
  async ( payload: HttpsPostSellerPayload): Promise<HttpsPostSellerResponse> => {
    const { data } = await api.post<HttpsPostSellerResponse>(
      '/api/seller/',
      payload
    )
    return data
  }

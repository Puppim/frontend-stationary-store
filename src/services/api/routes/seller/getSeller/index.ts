import api from 'services/apiClient'

import { HttpsPostResponseSeller } from './types'

export const getSeller =
  async (): Promise<HttpsPostResponseSeller> => {
    const { data } = await api.get<HttpsPostResponseSeller>(
      '/api/seller/'
    )
    return data
  }

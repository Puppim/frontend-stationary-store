export interface HttpsPostResponseSeller {
  count: number
  next: string|null
  previous: string|null
  results: SellerInterface[]
}

export interface SellerInterface {
  id: number
  first_name: string
  last_name: string
  email: string
  telephone: string
}

import api from 'services/apiClient'

import { HttpsPostSellerResponse, HttpsPostSellerPayload } from './types'

export const patchSellerById =
  async (id: number, payload: HttpsPostSellerPayload): Promise<HttpsPostSellerResponse> => {
    const { data } = await api.patch<HttpsPostSellerResponse>(
      `/api/seller/${id}/`,
      payload
    )
    return data
  }

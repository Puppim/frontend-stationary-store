export interface HttpsPostSellerPayload  {
  first_name?: string
  last_name?: string
  email?: string
  telephone?: string
}

export interface HttpsPostSellerResponse {
  id?: number
  first_name?: string
  last_name?: string
  email?: string
  telephone?: string
}

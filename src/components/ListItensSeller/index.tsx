import React from "react";
import { Link } from "react-router-dom";
import api from "services/apiClient";
import {SellerInterface} from 'services/api/routes/seller/getSeller/types'
import { Container, Itens } from './styles';
import Button from "components/Button";

function Del(pk: number) {
    api.delete(`api/seller/${pk}/`).then((response) => {
    if (response.status<=300 ) {
      window.location.reload();
    } else {
      alert("Item não pode ser excluido pois tem outro item associado");
    }
  }).catch(function (error) {
    if (error.response) {
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);}
      alert("Iten associado a outro iten não pode ser deletado")
    });
}

interface sellerProsInterface {
  seller : SellerInterface,
}


const TableRow: React.FunctionComponent<sellerProsInterface> = (props) => {
  return (
    <Container>
      <Itens>{props.seller.id}</Itens>
      <Itens>{props.seller.first_name}</Itens>
      <Itens>{props.seller.last_name}</Itens>
      <Itens>{props.seller.telephone}</Itens>
      <Itens>{props.seller.email}</Itens>
      <Itens>
        <Link to={"selleredit/" + props.seller.id} ><Button>
         Edit
        </Button>
        </Link>
      </Itens>
      <Itens>
        <Link to={"sellercommission/" + props.seller.id} ><Button>
         Comissão
        </Button>
        </Link>
      </Itens>
      <Itens>
        <Button onClick={() => Del(props.seller.id)} > DELETE</Button>
      </Itens>
    </Container>
  );
};
export default TableRow;
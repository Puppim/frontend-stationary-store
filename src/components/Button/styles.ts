import styled from 'styled-components';
import { shade } from 'polished';
// import sigInBackgroundImg from '../../assets/sign-in-background.png';

export const Container = styled.button`


/* StationaryRest Verde Claro */

background: #00585E;
/* dropshadowButton */

box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.08);

height: 56px;
border: 0;
padding: 0 16px;
width: 100%;

font-family: Roboto;
font-style: normal;
font-weight: 600;
font-size: 16px;
line-height: 133.19%;
color: #FFFFFF;

margin-top: 16px;
transition: background-color 0.2s;

&:hover {
  background: ${shade(0.2, '#00585E')};
}


`;

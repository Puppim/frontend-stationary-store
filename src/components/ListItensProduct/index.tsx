import React from "react";
import { Link } from "react-router-dom";
import api from "services/apiClient";
import {ProductInterface} from 'services/api/routes/product/getProduct/types'
import { Container, Itens } from './styles';
import Button from "components/Button";

function Del(pk: number) {
    api.delete(`api/product/${pk}/`).then((response) => {
    if (response.status<=300 ) {
      window.location.reload();
    } else {
      alert("Item não pode ser excluido pois tem outro item associado");
    }
  }).catch(function (error) {
    if (error.response) {
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);}
      alert("Iten associado a outro iten não pode ser deletado")
    });
}

interface productProsInterface {
  product : ProductInterface,
}


const TableRow: React.FunctionComponent<productProsInterface> = (props) => {
  return (
    <Container>
      <Itens>{props.product.id}</Itens>
      <Itens>{props.product.description}</Itens>
      <Itens>{props.product.commission}</Itens>
      <Itens>{props.product.unitary_value}</Itens>
      <Itens>
        <Link to={"productedit/" + props.product.id} ><Button>
         Edit
        </Button>
        </Link>
      </Itens>
      <Itens>
        <Button onClick={() => Del(props.product.id)} > DELETE</Button>
      </Itens>
    </Container>
  );
};
export default TableRow;
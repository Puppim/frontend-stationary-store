import React from "react";
import { Link } from "react-router-dom";
import api from "services/apiClient";
import {CustomersInterface} from 'services/api/routes/customers/getCustomers/types'
import { Container, Itens } from './styles';
import Button from "components/Button";

function Del(pk: number) {
    api.delete(`api/customers/${pk}/`).then((response) => {
    if (response.status<=300 ) {
      window.location.reload();
    } else {
      alert("Item não pode ser excluido pois tem outro item associado");
    }
  }).catch(function (error) {
    if (error.response) {
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);}
      alert("Iten associado a outro iten não pode ser deletado")
    });
}
interface customerProsInterface {
  customer : CustomersInterface
}

const TableRow: React.FunctionComponent<customerProsInterface> = (props) => { 
  return (
    <Container>
      <Itens>{props.customer.first_name} {props.customer.last_name}</Itens>
      <Itens>{props.customer.email}</Itens>
      <Itens>{props.customer.telephone}</Itens>
      <Itens>
        <Link to={"customeredit/" + props.customer.id} ><Button>
         Edit
        </Button>
        </Link>
      </Itens>
      <Itens>
        <Button onClick={() => Del(props.customer.id)} > DELETE</Button>
      </Itens>
    </Container>
  );
};
export default TableRow;
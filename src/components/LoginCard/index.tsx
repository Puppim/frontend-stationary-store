import React, { ButtonHTMLAttributes, useState } from 'react';

import { Visibility, VisibilityOff } from '@material-ui/icons';
import {
  LoginFrame,
} from './styles';

import Input from '../../components/Input';
import Button from '../../components/Button';
//Images
import { Form } from '@unform/web';

import { useCallback, useRef } from 'react';
import { FiMail, FiLock} from 'react-icons/fi';
import { FormHandles } from '@unform/core';
import * as Yup from 'yup';
import { useHistory } from 'react-router-dom';
import getValidationErrors from '../../utils/getValidationErros';

import { useToast } from '../../hooks/ToastContext';
import { useAuth } from '../../hooks/AuthContext';
import LinkStyled from '../LinkStyled/LinkStyled';

interface SignInFormData {
  username: string;
  password: string;
}



const LoginCard: React.FC = ({children}) => {

  localStorage.removeItem('responseCookie');
  
  const formRef = useRef<FormHandles>(null);
  const { signIn, user } = useAuth();
  const [showPass, setShowpass] = useState(true);
  const [errorPass, setErrorPass] = useState(false);

  const HandleShowPass = ()=>{
    setShowpass(!showPass);
  }

  const ErrorPass = ()=>{
    setErrorPass(!errorPass);
  }

  const { addToast } = useToast();
  const history = useHistory();

  const handleSubmit = useCallback(async (data: SignInFormData) => {
    try {
      formRef.current?.setErrors({});

      const schema = Yup.object().shape({
        username: Yup.string().required('email obrigatório'),
        password: Yup.string().min(6, 'Senha obrigatória'),
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      await signIn({
        username: data.username,
        password: data.password,
      });
    } catch (err) {

      if (err instanceof Yup.ValidationError) {
        const errors = getValidationErrors(err);

        formRef.current?.setErrors(errors);
        return;
      }

      ErrorPass();
      addToast({
        type: 'error',
        title: 'Erro na autenticacao',
        description: 'Ocorreu um errro ao fazer login',
      });
    }
  },
  [signIn, addToast, history]);


return(
  <>
  <LoginFrame>
      <div className="title">Papelaria Virtual</div>

      <Form ref={formRef} onSubmit={handleSubmit}>
        <Input className='username' name="username" icon={FiMail} placeholder="E-mail" />
        <Input  className='password' name="password" icon={FiLock} type={(showPass) ? "password":"text" } placeholder="Senha" > 
           <div onClick={HandleShowPass} className="passwordShow"> {showPass?<VisibilityOff></VisibilityOff> :<Visibility></Visibility>}</div> 
        </Input>
        {errorPass ?<div className="erropass">Senha incorreta. Tente novamente ou clique em "Nova senha" para redefini-la.</div>: null}
       
        <Button id='btnlogin' type="submit">Entrar</Button>
      </Form>
      {children}
  
      <br/>
      <div className="links">
          <div className="createAcount">  Não tem uma conta? <LinkStyled to={"/signup"}> Criar conta </LinkStyled>  </div>
          <div className="createAcount">  Esqueceu a senha?   <LinkStyled to={"/resetpassword"}>Nova senha  </LinkStyled> </div>
      </div>
  </LoginFrame>

  </>
)};

export default LoginCard;

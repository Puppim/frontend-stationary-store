import styled from 'styled-components';

export const LoginFrame = styled.div`
  margin-top:40px;

  display: flex;
  width: 599px;
  height: 738px;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  background: #F4F0EF;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.16);
  border-radius: 11.8925px;
  flex-direction: column;
  justify-content: center;
  margin-right:15px;


  /* @media only screen and (max-width: 900px) { */

  @media(max-width: 900px) {
    width: 100%;
    height: 100%;
  }

  .erropass{
    display: flex;
    width: 300px;
    font-size: 16px;
    color: #c53030;
  }


  .title{
      color: #00585E;
      margin-bottom:44px;
      font-family: Roboto;
      font-style: normal;
      font-weight: 300;
      font-size: 32px;
    }

  Button{
    margin-top: 28px;
    }

  .savePassword {
    margin-top: 28px;
  }
  .savePassword label {
    margin-left:8px;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 135%;
    color: #111111;
  }

  .links {
    display:flex;
    flex-direction: column;
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 16px;
    line-height: 135%;
    /* or 22px */

    display: flex;
    align-items: center;

    color: #8E8E8E;
  }
  .links  a {
      text-decoration: none;
  }

`;


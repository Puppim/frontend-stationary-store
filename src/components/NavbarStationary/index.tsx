
import React, { useEffect, useState } from 'react';


import { useAuth } from '../../hooks/AuthContext'

import {
  ContainerNav,
  NavbarFrame,
} from './styles';

import {
  Navbar,
  Nav,
  Figure,
} from "react-bootstrap";



import api from '../../services/apiClient';
import { Link } from 'react-router-dom';
import logo from 'assets/logopaper.png'
import { title } from 'process';

interface tittle{
  title: string|null;
}



const NavbarStationary: React.FC<tittle> = ({title}) => {



// const Header: React.FC= () => {

 
  const { signOut  } = useAuth();

  return (
    <>

    <NavbarFrame>

          <ContainerNav>
              <Navbar
              collapseOnSelect
                  bg="light"
                  variant="light"
                  expand="bg"
                  sticky="top"
                  className="bg-light justify-content-between"
                >
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Brand>
                <Figure.Image
                  width={100}
                  height={50}
                  alt="171x180"
                  src={logo}
                />
              </Navbar.Brand>
              <Navbar.Brand>
               {title}
              </Navbar.Brand>
                <Navbar.Collapse className="justify-content-between" id="responsive-navbar-nav">
                    <span className="SpanNavBegin" ></ span>
                   
                      <Nav.Link>
                      <Link to="/dashboard">
                        <b>Home</b>
                        </Link>
                     </Nav.Link>
                   
                    <Nav.Link><Link to="/customers">Clientes </Link></Nav.Link>
                    <Nav.Link><Link to="/commissionday">Comissão do Dia </Link></Nav.Link>
                    <Nav.Link><Link to="/seller"> Vendedores</Link></Nav.Link>
                     <Nav.Link><Link to="/sale">Vendas</Link></Nav.Link>
                    <Nav.Link> <Link to="/Product">Produtos</Link></Nav.Link>
                  <Nav.Link>  <Link to="/itens"> itens</Link></Nav.Link>
                      <Nav.Link>  <Link to="/"> 
                      <b className="square" onClick={() => signOut() }>
                                Sair
                      </b>
                      </Link>
                      </Nav.Link>
                    
                    <span className="SpanNavEnd" ></ span>
                    <Link to="/dashborad"></Link>
                    {/* </div> */}
                </Navbar.Collapse>
                <div className="cardButton">
    
                </div>
              </Navbar>
            </ContainerNav>
        </NavbarFrame>

    </>
  );
};

export default NavbarStationary;

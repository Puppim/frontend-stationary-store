import styled from 'styled-components';





export const NavbarFrame = styled.div`
  height: 80px;
  width: 100%;
  background-color: #F4F0EF ;
  /* max-width: 1375px; */
  display: flex;
  flex-direction: column;
  align-items: stretch;

  /* background: blue; */
  .bg-light{
    background: var(--color-background)!important;
    background: #F4F0EF !important;
  }
  color: #00585E;
`;


export const ContainerNav = styled.div`
  display: block;
  flex-grow: 0;
  flex-shrink: 1;
  flex-basis: auto;
  position: absolute;
  height: 80px;
  padding: 0px;
  width: 100%;

  margin: 0px 0px 0px 0px;
  /* background: var(--color-background); */
  /* background: red; */

  .navbar-brand
  {
      position: absolute;
      width: 80%;
      right: 220px;
      text-align: center;
      margin:0 auto;
      color: #00585E;
      font-family: 'Roboto';
      font-style: normal;
      font-weight: 800;
      font-size: 32px;
      line-height: 133.19%;
  }
 
nav {
  display: flex;
  flex-grow: 0;
  flex-direction: row;
  align-items: start;
  flex-shrink: 1;
  flex-basis: auto;
  color: #00585E;

}
img {
    margin-right: 80%;
  }

.SpanNavEnd {
  margin-left: 60px;
  margin-right: 120px;
  margin-top: 90px;
}

  .links a{
    display: flex;
    flex-direction: row;
    color: #112233;
  }


  a {
    font-family: Roboto;
    font-style: normal;
    font-weight: normal;
    font-size: 18px;
    line-height: 25px;
    /* color: #112233; */
    text-decoration: none;
    color: #00585E;
  }

 

  @media (max-width: 980px) {

      .navbar-brand {

        position: relative;
        right: 310px;
        top: 20px;
        height: 80px;
        padding: 0px;
        margin: 5px;
      }

      .cardButton {
          width: 200px;
            height: 100px;
            position: absolute;

            left: 80%;


        }

}

@media (max-width: 842px) {
  .cardButton {
          width: 200px;
            height: 100px;
            position: relative;

            left: 77%;


        }
}


@media (max-width: 420px) {

    .navbar-brand {

      position: relative;
      right: 220px;
      top: 20px;
      height: 80px;
      padding: 0px;
      margin: 5px;
  }

  .cardButton {
          width: 200px;
            height: 100px;
            position: relative;

            left: 50%;


        }

}
  @media (max-width: 380px) {


    .navbar-brand {

      position: relative;
      right: 145px;
      top: 20px;
      height: 80px;
      padding: 0px;
      margin: 5px;
  }


  .cardButton {
          width: 200px;
            height: 100px;
            position: relative;

            left: 50%;

        }



}

@media (max-width: 362px) {
  .cardButton {
          width: 200px;
            height: 100px;
            position: relative;
            left: 48%;
        }
}

@media (max-width: 330px) {
  .cardButton {
          width: 200px;
            height: 100px;
            position: relative;

            left: 43%;


        }
}

  a img {
    margin-right: 16px;
  }

  .cardButton {
      /* display: flex; */
      position: absolute;
      width: 200px;
      height: 100px;
      top: 0%;
      right: 0%;
      /* background: #F712F7; */
      //box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.16);

      overflow: hidden;

    }


    .cardButton .btn {
      position: absolute;
      top: 18%;
      left: 20%;
      width: 300px;
      height: 57px;
      /* background-color: rgba(52, 196, 143, 0.5); */
      background-color: rgba(52, 196, 143);
      color: #F2C94C;
      font-size: 16px;
      border: none;
      cursor: pointer;
      border-radius: 43px;
      text-align: left;
    }
    .cardButton .btn b {
      margin: 12px;
      margin-right: 45px;
    }

    .cardButton .btn:hover {
      background-color: rgba(0, 120, 0, 0.5);
    }

`;

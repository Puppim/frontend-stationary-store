import React, {useEffect, useState} from "react";
import { Container, Modalstyle, FrameButton} from './styles'
import Button from '../Button';

import Loader from '../loader';

interface Modalprops{
  className: string;
  buttonName: string;
  buttonAccept?: string;
  width?:string;
  height?: string;
  btnAble?: boolean;
  
  doglebtn?: () => void;
  onClose: () => void;
  onClick?: () => void;
}



const Modal: React.FC<Modalprops> = ({className, width, btnAble, height, buttonName,buttonAccept, onClose , onClick, children, ...rest}) => {

    return(
      
       <Container width={width?width:"auto"} height={height?height:"auto"} {...rest} >
        <Modalstyle >
            {children}
            {btnAble?  <Loader></Loader>: null} 
            
            <FrameButton>
               {buttonAccept? <Button disabled={btnAble} onClick={onClick}>{buttonAccept}</Button> : null} 
                <Button onClick={onClose}>{buttonName}</Button>
            </FrameButton>
        </Modalstyle>
            
              
           
        </Container>
       
    )
}

export default Modal;

import styled from 'styled-components';



interface ContainerStyle{
  width: string;
  height: string;
}


export const Container = styled.div<ContainerStyle>`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  position: absolute;
  top: 0;
  left: 0;
  z-index: 2000;
  /* background-color: rgb(0,0,0, 0.8); */
  background-color: transparent;
   display: flex;
  justify-content: center;
  align-items:center;
`;

export const Modalstyle = styled.div`
    /* background-color: #fff; */
    background-color: #F4F0EF;
    width: auto;
    height: auto;
    border-radius: 20px;
    display: flex;
    justify-content: center;
    align-items:center;
    flex-direction: column;
    padding:6rem;

`
export const FrameButton = styled.div`
 
    width: auto;
    height: 20%;

   button{
    background: grey;
   }

`

export const Button = styled.button`
   z-index: 20;
   color: #000;
   margin: 20px;
   background: grey;

   

`;



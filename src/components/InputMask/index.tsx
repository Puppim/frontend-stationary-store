import React, {
  InputHTMLAttributes, useEffect, useRef, useState, useCallback,
} from 'react';
import ReactInputMask, { Props as InputProps } from 'react-input-mask';

import { boolean } from 'yup';

import { IconBaseProps } from 'react-icons';
import { FiAlertCircle } from 'react-icons/fi';
import { useField } from '@unform/core';
import { Container, Error } from './styles';
import Input from '../../components/Input';


interface Props extends InputProps {
  name: string;
  icon?: React.ComponentType<IconBaseProps>;
}

export default function InputMask({ name, icon: Icon, ...rest }: Props) {

  
  const [isFocused, setIsFocused] = useState(false);
  const [isField, setIsField] = useState(false);
 
  const inputRef = useRef(null);
  const { fieldName, registerField, defaultValue, error } = useField(name);




  const handleFocus = useCallback(() => {
    setIsFocused(true);
  }, []);



  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
      setValue(ref: any, value: string) {
        ref.setInputValue(value);
      },
      clearValue(ref: any) {
        ref.setInputValue('');
      },
    });
  }, [fieldName, registerField]);

  const handleInputBlur = useCallback(() => {
    setIsFocused(false);
    setIsField(!!inputRef.current); // o primeiro ! nega mas tranforma e boolea o segundo volta para o valor correto
  }, []);

  return (
    <>
    <Container isErrored={!!error} isField={isField} isFocused={isFocused}>
    {Icon && <Icon size={20} />}
   
    <ReactInputMask id="inputstyle" ref={inputRef} defaultValue={defaultValue} {...rest} />

    {error && (
        <Error title={error}>
          <FiAlertCircle color="#c53030" size={20} />
        </Error>
      )}
   
   </Container>
    </>
  );
};
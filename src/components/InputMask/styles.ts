import styled, { css } from 'styled-components';
import Tooltip from '../Tooltip';
// import { shade } from 'polished';
// import sigInBackgroundImg from '../../assets/sign-in-background.png';

interface ContainerProps {
  isFocused: boolean;
  isField: boolean;
  isErrored: boolean;
}

export const Error = styled(Tooltip)`
height: 20px;
margin-left: 16px;

svg{
  margin: 0;
}

span{
  background: #c53030;
  color: #232129;

  &::before {
    border-color: #c53030 transparent;
  }
}
`;

export const Container = styled.div<ContainerProps>`
  display: flex;
  background: #FFFFFF;
  border-radius: 10px;
  padding: 16px;
  width: 100%;

  color: #232129;
  border: 2px solid #FFFFFF;

  & + div{
        margin-top: 8px;
      }

  ${(props) => props.isErrored && css`
    border-color:#c53030;

  `}


  ${(props) => props.isFocused && css`
    color: #34C48F;
    border-color: #34C48F;
  `}

  ${(props) => props.isField && css`
    color: #34C48F;

  `}

  /* .inputreal{
    visibility:hidden;
  } */


  #inputstyle {
  flex: 1;
  margin-left: 16px;
  background: transparent;
  border: 0;


  color: #232129;


  &::placehold {
    color: #112129;
      }



    }

`;

import * as React from 'react';
import api from "services/apiClient";
import { useToast } from 'hooks/ToastContext';
import {CustomersInterfacePayload} from 'services/api/routes/customers/getCustomersById/types'

import Button from 'components/Button';
import Input  from 'components/Input';

interface Props {
    customer: CustomersInterfacePayload;
    onChange: (fieldName: string, value: string) => void;
    onSave: () => void;
}

export const CustomerForm: React.FunctionComponent<Props> = ({ customer,onChange, onSave}) => { 
    return (
        <form>
            <h1>Clientes</h1>

            <Input
                name="Nome"
                value={customer?.first_name}
            />

            <Input
                name="Sobrenome"
                value={customer?.last_name}
            />

            <Input
                name="email"
                value={customer?.email}
            />

            <Input
                name="Telefone"
                value={customer?.telephone}
            />

            <Button
                name="Salvar"
                className="btn btn-success mt-2"
                onClick={onSave}
            />
        </form>
    );
};
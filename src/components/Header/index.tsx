import React from 'react';

import { Container } from './styles';

interface title{
  name: string;
}
const Header: React.FC <title> = ( { name }) => {
  return (
    <>

      <Container>
          <h1>{name}</h1>
    </Container>
    </>
  );
};
export default Header;

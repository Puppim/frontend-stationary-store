import React from "react";
import { Link } from "react-router-dom";
import api from "services/apiClient";
import {SaleInterface} from 'services/api/routes/sale/getSale/types'
import { Itens } from './styles';
import Button from "components/Button";
import {formatDate} from "utils/formatDate"
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from "@material-ui/core";
import { Edit , } from '@material-ui/icons'
import {FiEdit, FiTrash, FiEye, FiEyeOff} from 'react-icons/fi';
import {Collapse, Box} from "@material-ui/core";


function Del(pk: number) {
    api.delete(`api/sale/${pk}/`).then((response) => {
    if (response.status<=300 ) {
      window.location.reload();
    } else {
      alert("Item não pode ser excluido pois tem outro item associado");
    }
  }).catch(function (error) {
    if (error.response) {
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);}
      alert("Iten associado a outro iten não pode ser deletado")
    });
}

interface saleProsInterface {
  sale : SaleInterface[],
}


const ListItensSale: React.FunctionComponent<saleProsInterface> = ({sale}) => {



  function CollapsibleTable(e:SaleInterface) {
    return (
    <React.Fragment>
      <TableRow >
      <TableCell>
      
       
      </TableCell>
      <TableCell>Nota Fiscal</TableCell>
          <TableCell align="right">Cliente</TableCell>
          <TableCell align="right">Vendedor</TableCell>
          <TableCell align="right">Data da Venda</TableCell>
          <TableCell align="right">Valor</TableCell>
          <TableCell align="right">Opções</TableCell>
          <TableCell align="right"></TableCell>
    </TableRow>
    <TableRow>
      <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
        <Collapse in={open} timeout="auto" unmountOnExit>
          <Box sx={{ margin: 1 }}>
              History
            <Table size="small" aria-label="purchases">
              <TableHead>
                <TableRow>
                  <TableCell>Date</TableCell>
                  <TableCell>Customer</TableCell>
                  <TableCell align="right">Amount</TableCell>
                  <TableCell align="right">Total price ($)</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
              {sale.map((e)=> 
                <TableRow key={e.created_at} >
                
                    <TableCell align="left">{e.number_nfe}</TableCell>
                    <TableCell align="right">{e.customers.first_name}</TableCell>
                    <TableCell align="right">{e.seller.first_name}</TableCell>
                    <TableCell align="right">{formatDate(e.created_at)}</TableCell>
                    <TableCell align="right">R$ {e.total_amount}</TableCell>
                    <TableCell align="right">  
                      <Link to={"saleedit/" + e.id} > 
                        <FiEdit color="#00585E" size={20} ></FiEdit>
                      </Link>
                      <Button onClick={()=>setOpen(true)}>Collapsible</Button>
                      </TableCell>
                      <TableCell align="right"> 
                      <FiTrash onClick={() => Del(e.id)}color="#BE0000" size={20}  ></FiTrash>
                    </TableCell>
                </TableRow> )}
              </TableBody>
            </Table>
          </Box>
        </Collapse>
      </TableCell>
    </TableRow>
  </React.Fragment>
    );
  }

  const [open, setOpen] = React.useState(false);
  return (

    <TableContainer component={Paper}>
    <Table aria-label="collapsible table">
      <TableHead>
        <TableRow>
          <TableCell />
         
          <TableCell align="right">Calories</TableCell>
          <TableCell align="right">Fat&nbsp;(g)</TableCell>
          <TableCell align="right">Carbs&nbsp;(g)</TableCell>
          <TableCell align="right">Protein&nbsp;(g)</TableCell>
        </TableRow>
      </TableHead>
      <TableBody>
      {sale.map((e)=> 
         <TableRow key={e.created_at} >
          <TableCell>  <Button
          onClick={() => (setOpen(!open)) }
        >
          ver itens
         </Button>
         {open&&CollapsibleTable(e)}
          {open ? <FiEye /> : <FiEyeOff />}</TableCell>
            <TableCell align="left">{e.number_nfe}</TableCell>
            <TableCell align="right">{e.customers.first_name}</TableCell>
            <TableCell align="right">{e.seller.first_name}</TableCell>
            <TableCell align="right">{formatDate(e.created_at)}</TableCell>
            <TableCell align="right">R$ {e.total_amount}</TableCell>
            <TableCell align="right">  
              <Link to={"saleedit/" + e.id} > 
                <FiEdit color="#00585E" size={20} ></FiEdit>
              </Link>
              <Button onClick={()=>setOpen(true)}>Collapsible</Button>
              </TableCell>
              <TableCell align="right"> 
              <FiTrash onClick={() => Del(e.id)}color="#BE0000" size={20}  ></FiTrash>
            </TableCell>
        </TableRow> )}
      </TableBody>
    </Table>
  </TableContainer>

  // <TableContainer component={Paper}>
  //   <Table aria-label="simple table">
  //     <TableHead>
  //       <TableRow>
          // <TableCell>Nota Fiscal</TableCell>
          // <TableCell align="right">Cliente</TableCell>
          // <TableCell align="right">Vendedor</TableCell>
          // <TableCell align="right">Data da Venda</TableCell>
          // <TableCell align="right">Valor</TableCell>
          // <TableCell align="right">Opções</TableCell>
          // <TableCell align="right"></TableCell>
  //       </TableRow>
  //     </TableHead>
  //     <TableBody>
  //      {sale.map((e)=> 
  //        <TableRow key={e.created_at} >
         
  //           <TableCell align="left">{e.number_nfe}</TableCell>
  //           <TableCell align="right">{e.customers.first_name}</TableCell>
  //           <TableCell align="right">{e.seller.first_name}</TableCell>
  //           <TableCell align="right">{formatDate(e.created_at)}</TableCell>
  //           <TableCell align="right">R$ {e.total_amount}</TableCell>
  //           <TableCell align="right">  
  //             <Link to={"saleedit/" + e.id} > 
  //               <FiEdit color="#00585E" size={20} ></FiEdit>
  //             </Link>
  //             <Button onClick={()=>setOpen(true)}>Collapsible</Button>
  //             </TableCell>
  //             <TableCell align="right"> 
  //             <FiTrash onClick={() => Del(e.id)}color="#BE0000" size={20}  ></FiTrash>
  //           </TableCell>
  //       </TableRow> )}
  //     </TableBody>
  //   </Table>
  //   </TableContainer>
  
);
};


export default ListItensSale;


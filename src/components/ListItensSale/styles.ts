import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  width: auto;
  height: auto;

`;



export const Itens = styled.div`
  font-family: 'Roboto';
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 133.19%;
  /* or 21px */

  display: flex;
  align-items: center;
  text-align: right;
  a.{
    text-decoration: none;

    color: #2C3749;

    &:focus, &:hover, &:visited, &:link, &:active {
        text-decoration: none;
    }
  }
  /* $XPTO/brand/primary */

  color: #00585E;

`;
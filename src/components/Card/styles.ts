import styled from 'styled-components';

interface ContainerStyle{
  width:  string;
  height: string;
}

export const Container = styled.div<ContainerStyle>`
display: flex;
width: ${(props) => props.width};
height: ${(props) => props.height};
background: #F4F0EF;
box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.16);;
border: 1px solid #F4F0EF;
box-sizing: border-box;
border-radius: 16px;
flex-direction: column;
align-content: center;

@media( max-width: 640px){
  width: 100%;
  height:100%;

}

img{
  margin:8px;
}


`;


export const Title = styled.div`
margin:8px;
margin-top: 20px;
font-family: Roboto;
font-style: normal;
font-weight: 500;
font-size: 24px;
line-height: 140%;
display: flex;
align-items: center;
color: #333333;
`;

export const ContainerText = styled.div`
margin: 36px 36px;
font-family: Roboto;
font-style: normal;
font-weight: 500;
font-size: 16px;
line-height: 140%;
display: flex;
color: #2C3749;
`;

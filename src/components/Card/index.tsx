import React from 'react';

import { Container, Title, ContainerText } from './styles';


interface CardContents{
  name: string;
  image?: string;
  title?: string;
  width:string;
  height: string;
}

const Card: React.FC<CardContents> = ({ image, title,width,height,children, ...rest }) => (
  <Container width={width} height={height} {...rest}>

    <Title>
    { image &&  <img src={image} width="50" height="50"  alt="First slide" ></img>}
      {title}
    </Title>
    <ContainerText>
      {children}
    </ContainerText>



  </Container>
);

export default Card;

import React, { ButtonHTMLAttributes } from 'react';

import CookieConsent, { Cookies, resetCookieConsentValue, getCookieConsentValue } from "react-cookie-consent";

const CookiesAllow: React.FC = () =>{

  const ckeckCookie = getCookieConsentValue('responseCookie')
  if(ckeckCookie === 'false'){
    resetCookieConsentValue('responseCookie')
  }
  
  return(

    <CookieConsent
            flipButtons
              enableDeclineButton
              location="bottom"
              buttonText="Sim, aceito"
              declineButtonText="Não"
              declineButtonStyle={{ 
                background: " #34C48F", color: "#2C3749", fontSize: "12px" }}
              cookieName="responseCookie"
              style={{ background: "#2C3749",fontSize: "12px" }}
              buttonStyle={{ 
                background: " #34C48F", color: "#2C3749", fontSize: "13px" }}
              expires={256}
    
              onAccept={(acceptedByScrolling) => {
                if (acceptedByScrolling) {
                  alert("Accept was triggered by user scrolling");
                } else {
                 
                }
              }}
            >
              A StationaryRest utiliza cookies e outras tecnologias semelhantes para melhorar a sua experiência em nossa plataforma. Ao fazer login, você aceita a utilização dessas tecnologias.
              
              
    </CookieConsent>
  );
}
 

export default CookiesAllow;

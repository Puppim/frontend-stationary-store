import styled from 'styled-components';

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  position: absolute;
  top: -5%;
  left: 0px;
  z-index: 100;
  /* background-color: rgb(0,0,0, 0.8); */
  background-color: transparent;
  display: flex;
  justify-content: center;
  align-items:center;
`;

export const Modalstyle = styled.div`
   /* background-color: #fff; */
   background-color: rgb(0,0,0, 0.8);
   color: #fff;
    width: auto;
    height: auto;
    display: flex;
    justify-content: center;
    align-items:center;
    flex-direction: column;
    padding:6rem;


`
export const TextCoockie = styled.div`
  margin:20px;
  font-size: 1.5rem;
`
export const FrameButton = styled.div`
    display: flex;
    flex-direction: row;
    width: auto;
    height: 20%;

   button{
    margin:15px;
    background: grey;
    font-size: 1rem;
   }

   @media (max-width: 800px) {
      flex-direction: column;
   }

`

export const Button = styled.button`
   z-index: 200;
   color: #000;
   margin: 20px;
   background: grey;


`;



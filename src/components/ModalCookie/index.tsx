import React from "react";
import { Container, Modalstyle, FrameButton, TextCoockie} from './styles'
import Button from '../Button';


import { useAuth } from '../../hooks/AuthContext'
import { useHistory } from 'react-router-dom';

interface Modalprops{
  className: string;
  onClose: () => void;
//   onClick: () => void;
}

interface CookieState{
    accept: boolean;
  }



const ModalCookie: React.FC<Modalprops> = ({className, onClose , children, ...rest}) => {

    const history = useHistory();

    const { signOut  } = useAuth();

    const goOut = () =>{
        signOut();
        history.push("/")
    }

    const accept = () => {
                localStorage.setItem('@StationaryRestrest:acceptcookie', "true");
                onClose();
    }

    return(
        <Container id="tips"  {...rest}>
            <Modalstyle>
                {children}
                <TextCoockie> <b>Diga-nos se concorda com o uso de cookies</b> </TextCoockie>
                <TextCoockie>  Nós usamos cookies para lhe proporcionar a melhor experiência online. Diga-nos se concorda com o uso de cookies</TextCoockie>
                   
                <FrameButton>
                 
                   
                    <Button onClick={ () => accept() }> Sim, concordo </Button>
                    <Button onClick={() => goOut() } > Não concordo, sair </Button>
                </FrameButton>
            </Modalstyle>
        </Container>
    )
}

export default ModalCookie;

import React from "react";
import { Link } from "react-router-dom";
import api from "services/apiClient";
import {CommissiondayInterface} from 'services/api/routes/commissionday/getCommissionday/types'
import { Container, Itens } from './styles';
import Button from "components/Button";

function dayofweek(num: number) {
  switch (num) {
    case 0:
      return "Segunda-feira";
      break;

    case 1:
      return "Terça-feira";
      break;

    case 2:
      return "Quarta-feira";
      break;
    case 3:
      return "Quinta-feira";
      break;

    case 4:
      return "Sexta-feira";
      break;

    case 5:
      return "Sábado";
      break;

    case 6:
      return "Domingo";
      break;

    }
}

function Del(pk: number) {
    api.delete(`api/commissionday/${pk}/`).then((response) => {
    if (response.status<=300 ) {
      window.location.reload();
    } else {
      alert("Item não pode ser excluido pois tem outro item associado");
    }
  }).catch(function (error) {
    if (error.response) {
      console.error(error.response.data);
      console.error(error.response.status);
      console.error(error.response.headers);}
      alert("Iten associado a outro iten não pode ser deletado")
    });
}

interface commissiondayProsInterface {
  commissionday : CommissiondayInterface,
}


const TableRow: React.FunctionComponent<commissiondayProsInterface> = (props) => {
  return (
    <Container>
      <Itens className="short">{props.commissionday.id}</Itens>
      <Itens>{dayofweek(props.commissionday.days)}</Itens>
      <Itens>{props.commissionday.max_commission}</Itens>
      <Itens>{props.commissionday.min_commission}</Itens>
      <Itens>{props.commissionday.updated_at}</Itens>
      <Itens>{props.commissionday.created_at}</Itens>
      <Itens>
        <Link to={"commissiondayedit/" + props.commissionday.id} ><Button>
         Edit
        </Button>
        </Link>
      </Itens>
      <Itens>
        <Button onClick={() => Del(props.commissionday.id)} > DELETE</Button>
      </Itens>
    </Container>
  );
};
export default TableRow;
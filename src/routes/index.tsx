import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';

import SignIn from '../pages/Signin';
import ResetPassword from '../pages/ResetPassword';
import ResetPasswordSubmit from '../pages/ResetPasswordSubmit';
import SignUp from '../pages/SignUp';
import Customers from '../pages/Customers';
import CustomersEdit from '../pages/CustomersEdit';
import Sale from '../pages/Sale';
import SaleEdit from '../pages/SaleEdit';
import Product from '../pages/Product';
import Productlist from '../pages/Productlist';
import ProductEdit from '../pages/ProductEdit';
import Seller from '../pages/Seller';
import SellerEdit from '../pages/SellerEdit';
import CommissionDay from '../pages/CommissionDay';
import CommissionDayEdit from '../pages/CommissionDayEdit';
import SellerCommission from '../pages/SellerCommission';
import SaleNew from '../pages/SaleNew';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/resetpassword" component={ResetPassword} />
    <Route path="/" exact component={SignIn} />
    <Route path="/signup" component={SignUp} />
    <Route path="/customeredit/:id" component={CustomersEdit} isPrivate />
    <Route path="/customers" component={Customers} isPrivate />
    <Route path="/sale" component={Sale} isPrivate />
    <Route path="/seller" component={Seller} isPrivate />
    <Route path="/itens" component={Productlist} isPrivate />
    <Route path="/newsale" component={SaleNew} isPrivate />
    <Route path="/product" component={Product} isPrivate />
    <Route path="/commissionDay" component={CommissionDay} isPrivate />
    <Route path="/productedit/:id" component={ProductEdit} isPrivate />
    <Route path="/sellercommission/:id" component={SellerCommission} isPrivate />
    <Route path="/commissiondayedit/:id" component={CommissionDayEdit} isPrivate />
    <Route path="/saleedit/:id" component={SaleEdit} isPrivate />
    <Route path="/selleredit/:id" component={SellerEdit} isPrivate />
    <Route path="/api/password-reset/:uidb64/:token" component={ResetPasswordSubmit} />
  </Switch>
);

export default Routes;

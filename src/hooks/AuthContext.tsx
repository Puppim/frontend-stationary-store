import React, {
  createContext, useCallback, useContext, useState,
} from 'react';

import Axios from 'axios';

import api from '../services/apiClient';

interface SignInCredentials{
  username: string;
  password: string;
}

interface AuthContextData{
  user: object;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
}

interface AuthState{
  token: string;
  user: object;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>(() => {
    const token = localStorage.getItem('@StationaryRest:token');
    const user = localStorage.getItem('@StationaryRest:user');


    if (token && user) {
      try {
        return { token, user: JSON.parse(user) };
      } catch (error) {
        localStorage.removeItem('@StationaryRest:token');
        localStorage.removeItem('@StationaryRest:user');
        return { token: '', user: null };
      }
      
    }
    return {} as AuthState;
  });
 
  const signOut = useCallback(() => {
    localStorage.removeItem('@StationaryRest:token');
    localStorage.removeItem('@StationaryRest:user');

    setData({} as AuthState);
  }, []);

  const signIn = useCallback(async ({ username, password }) => {
    const response = await api.post('api/token/', {
      username,
      password,
    });

    const { access } = response.data;
    const user = username;
    const token = access;
    console.log(response)
    localStorage.setItem('@StationaryRest:token', token);
    localStorage.setItem('@StationaryRest:user', JSON.stringify(user));
    setData({ token, user});
  }, []);



  return (
    <AuthContext.Provider value={{ user: data.user, signIn, signOut }}>
      {children}
    </AuthContext.Provider>
  );
};

function useAuth(): AuthContextData {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be used within a AuthProvider');
  }
  return context;
}

export { AuthProvider, useAuth };

import { createGlobalStyle } from 'styled-components';
// yarn add styled-components
// yarn add @types/styled-components -D

export default createGlobalStyle`

:root {

font-size: 60%;
--color-background: #DADADA;;
--color-background-white: #FFFFFF;
--color-green-dark: #1A865E;
--color-orange-dark: #cc6600;
--color-green: #34C48F;
--color-orange: #00585E;
--color-gray-dark: #333333;
--color-gray-light: #8E8E8E;
--color-blue-dark: #2C3749;
}


  * {
      margin:0;
      padding: 0;
      box-sizing: border-box;
      outline:0;
  }

  body {
    background: var(---color-baground);
    color: var(---color-blue-dark);
    -webkit-font-smoothing: antialiased;

  }

  body, input, button {
    /* font-family: 'Roboto', serif; */
    
    font-family: Roboto;
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 133.19%;
  }

  h1, h2, h3, h4, h5, h6, strong {
    font-weight: 500;
  }

  button {
    cursor: pointer;
  }


  @media ( min-width: 700px) {
    :root {
      font-size: 80.5%;
    }
  }



`;

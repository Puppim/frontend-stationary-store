import React from 'react';

import CookieConsent, { Cookies, resetCookieConsentValue } from "react-cookie-consent";

import { BrowserRouter as Router } from 'react-router-dom';
import GlobalStyle from './styles/global';

// import SignIn from './pages/Signin';

import Routes from './routes';

import arroba from './assets/arroba.png'
// import SignUp from './pages/SignUp';

// import { AuthProvider } from './hooks/AuthContext';
// import { ToastProvider } from './hooks/ToastContext';

import AppProvider from './hooks';
import CookiesAllow from './components/CookiesAllow';



const App: React.FC = () => {



  return(

    <>   

      <Router>
      <GlobalStyle />
        <AppProvider>
       
          <Routes />
          <CookiesAllow></CookiesAllow>
        
        </AppProvider>
       
      </Router>
  
    </>
  );

}

export default App;

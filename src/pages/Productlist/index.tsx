import React, { useState, useCallback, useRef } from 'react';
import {Link, useParams} from 'react-router-dom';
import { Container , Content, Header } from './styles';
import { FiHash} from 'react-icons/fi';
import Input from 'components/Input';
import * as Yup from 'yup';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { ProductlistInterfacePayload }  from 'services/api/routes/productlist/getProductlistById/types';
import { postProductlist } from 'services/api/routes/productlist/registerProductlist'
import Button from 'components/Button';
import NavbarStationary from 'components/NavbarStationary';

interface RouteParams {
  id: string
}

const ProductlistEdit: React.FC = () => {

  const params = useParams<RouteParams>();
  const history = useHistory();
  const { signOut  } = useAuth();
  const [productlist, setProductlist] = useState<ProductlistInterfacePayload>()
  const formRef = useRef<FormHandles>(null);

const handleclearfile = () =>{
  setProductlist({})
  }


const handleSubmit = useCallback(async (data: ProductlistInterfacePayload) => {
  try {
    formRef.current?.setErrors({});

    const schema = Yup.object().shape({
      sale: Yup.string().required('obrigatório'),
      product: Yup.string().required('brigatório'),
      quantity: Yup.string().required('obrigatório'),

    });

    await schema.validate(data, {
      abortEarly: false,
    });
    await postProductlist(data);
    history.push('/');
  } catch (err) {
    if (err instanceof Yup.ValidationError) {
       console.log(err)
      return;
    }

  }
},
[history]);

return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={"Produtos"}></NavbarStationary>
    </Header>
    <Content>
            <Form ref={formRef} onSubmit={handleSubmit}>
                <h1></h1>
                <Input name="sale" icon={FiHash} onFocus={()=>handleclearfile()} placeholder={"Venda ID"}    />
                <Input name="product" icon={FiHash} onFocus={()=>handleclearfile()} placeholder={"Produto ID"} />
                <Input name="quantity" icon={FiHash} onFocus={()=>handleclearfile()} placeholder={"Quantidade"}/>
                <Button  type="submit">Salvar</Button>
              </Form>

              <Link to={'/'}><Button  type="submit">Voltar</Button></Link>
    </Content>
    
  </Container>
  
  </>
);
};

export default ProductlistEdit;

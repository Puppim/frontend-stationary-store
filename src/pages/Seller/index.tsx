import React, { useState, useEffect } from 'react';

import { Container , Content, Header, Itenslist} from './styles';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { getSeller }  from 'services/api/routes/seller/getSeller';
import { SellerInterface }  from 'services/api/routes/seller/getSeller/types';
import ListItensSeller  from 'components/ListItensSeller';
import NavbarStationary from 'components/NavbarStationary';

const Seller: React.FC = () => {

  const [seller, setSeller] = useState<SellerInterface[]>([])
  const fetchSeller = async () => {
    try {
      const response = await getSeller()
      setSeller(response.results)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    fetchSeller()
  }, [])

return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={"Vendedores"}></NavbarStationary>
    </Header>
    <Content>
    {seller.map(e=>
    <Itenslist>
       <ListItensSeller seller={e} ></ListItensSeller>
    </Itenslist>
    )}
    </Content>
  </Container>
  </>
);
};

export default Seller;

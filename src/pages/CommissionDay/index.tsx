import React, { useState, useEffect } from 'react';

import { Container , Content, Header, Footer, Itenslist} from './styles';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { getCommissionday }  from 'services/api/routes/commissionday/getCommissionday';
import { CommissiondayInterface }  from 'services/api/routes/commissionday/getCommissionday/types';
import ListCommissionday  from 'components/ListItensCommissionday';
import NavbarStationary from 'components/NavbarStationary';

const Commissionday: React.FC = () => {


  const history = useHistory();
  const { signOut  } = useAuth();
  const [commissionday, setCommissionday] = useState<CommissiondayInterface[]>([])

  const fetchCommissionday = async () => {
    try {
      const response = await getCommissionday()
      setCommissionday(response.results)
    } catch (error) {
      console.log(error)
    }
  }
  // Effect
  useEffect(() => {
    fetchCommissionday()
  }, [])



return(
  <>
  <Container>

    <Header>
    <NavbarStationary title={"Comissão"}></NavbarStationary>
    </Header>
 
    <Content>
    {commissionday.map(e=>
    <Itenslist>
       <ListCommissionday commissionday={e} ></ListCommissionday>
    </Itenslist>
    )}
    </Content>
  
   

  </Container>
  
  </>
);
};

export default Commissionday;

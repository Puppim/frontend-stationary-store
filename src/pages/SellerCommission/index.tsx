import React, { useState, useEffect } from 'react';

import { Container , Content, Header, Itenslist} from './styles';
import {useParams} from 'react-router-dom';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { getCommissionSellerById }  from 'services/api/routes/commission/getCommissionSellerById';
import { CommissionSellerInterface }  from 'services/api/routes/commission/getCommissionSellerById/types';
import NavbarStationary from 'components/NavbarStationary';

interface RouteParams {
  id: string
}

const SellerCommision: React.FC = () => {

  const params = useParams<RouteParams>();
  const history = useHistory();
  const { signOut  } = useAuth();
  const [total, setTotal] = useState(0)
  const [commission, setCommission] = useState<CommissionSellerInterface[]>([])

  const fetchCommission = async (id: number) => {
    try {
      const response = await getCommissionSellerById(id)
      setCommission(response.results)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    fetchCommission(+params.id);
  }, [])


  const totalsum = () =>{
    var total = commission.reduce((sum, seller) => {
      return sum + (parseFloat(seller.commission_amount));
    }, 0);
    setTotal(total)
  }
  useEffect(() => {
    totalsum();
  }, [commission])


return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={"Vendedores"}></NavbarStationary>
    </Header>
 
    <Content>

    {commission.length>0?
    commission.map(e=>
    <Itenslist>
        <div className="commission">ID: {e.id} - </div>
       <div className="commission">Vendedor: {e.seller} - </div>
       <div className="commission">Comissão: R$ {e.commission_amount} - </div>
       <div className="commission">Lista ID:{e.productList} - </div>
       <div className="commission">Data:{e.created_at}</div>
    </Itenslist>
    ): <div className="nullitem">
      Sem comissão para esse vendedor.
    </div>
    }

    {
      <div className="commission">Comissão Total do Vendedor: R$ {total.toFixed(2)}</div>
    }
    </Content>
  
  

  </Container>
  
  </>
);
};

export default SellerCommision;

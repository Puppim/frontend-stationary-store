import React, { useCallback, useRef } from 'react';
import {
  FiMail,
} from 'react-icons/fi';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import * as Yup from 'yup';
import { Link, useHistory } from 'react-router-dom';
import getValidationErrors from '../../utils/getValidationErros';
import api from '../../services/apiClient';
import { useToast } from '../../hooks/ToastContext';
import { Container, Content } from './styles';
import Input from '../../components/Input';
import Button from '../../components/Button';

interface ResetPasswodRequestFormData {
  email: string;
}

const ResetPasswodRequest: React.FC = () => {
  const formRef = useRef<FormHandles>(null);
  const { addToast } = useToast();
  const history = useHistory();
  const handleSubmit = useCallback(async (data: ResetPasswodRequestFormData) => {
    try {
      formRef.current?.setErrors({});
      const schema = Yup.object().shape({
        email: Yup.string().required('Email obrigatório').email('Digite um email válido'),
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      await api.post('/api/request-reset-email/', data);
      history.push('/');

      addToast({
        type: 'success',
        title: 'Cadastro realizado',
        description: 'Enviamos um link para seu email para que possa alterar sua senha',
      });
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errors = getValidationErrors(err);

        formRef.current?.setErrors(errors);
        return;
      }

      addToast({
        type: 'error',
        title: 'Erro no cadastro',
        description: 'Ocorreu um errro ao fazer o cadastro',
      });
    }
  }, [addToast, history]);

  return (
    <Container>
        <header className="navbar">
      </header>



      <Content>


        <div className="formreset">
        <Form ref={formRef} onSubmit={handleSubmit}>
          <h1>Alterar senha</h1>

          <Input name="email" icon={FiMail} placeholder="E-mail" />


          <Button type="submit">Enviar</Button>
        </Form>

        </div>

        <Link to="/">
          <Button type="submit">voltar</Button>
        </Link>


      </Content>

    </Container>
  );
};

export default ResetPasswodRequest;

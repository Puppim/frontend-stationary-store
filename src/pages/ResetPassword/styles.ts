import styled from 'styled-components';
import { shade } from 'polished';





export const Container = styled.div`
  height: 100vh;
  display: flex;

  flex-direction: column;


  .navbar {
    background: #F4F0EF;
    img{
      margin-left:30px;
    }
  }

`;

export const Content = styled.div`
display: flex;
flex-direction: column;
align-items: center;
justify-content: center;
margin: 30px;
width: 100%;


.formreset{
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: #F4F0EF;
  width: 500px;

  border: 1px solid #F4F0EF;
  box-sizing: border-box;
  border-radius: 8px;
}

@media only screen and (max-width: 700px) {

  margin: 20px 0px 10px 0px;
    .formreset{
    width: 100%;
    }
  }


form {
  margin: 80px 0;
  width: 300px;
  text-align: center;

  h1 {
    margin-bottom: 24px;
  }



  a {
    color: #F4EDE8;
    display: block;
    margin-top: 24px;
    text-decoration: none;
    transition: color 0.2s;

    &:hover {
      color: ${shade(0.2, '#F4EDE8')}
    }
  }


}
> a {
    color: #34C48F;
    display: block;
    margin-top: 24px;
    text-decoration: none;
    transition: color 0.2s;

    svg {
      margin-right: 16px;
    }

    &:hover {
      color: ${shade(0.2, '#34C48F')}
    }
  }



`;

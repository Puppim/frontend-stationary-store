import React, { useState, useEffect, useCallback, useRef } from 'react';
import {Link, useParams} from 'react-router-dom';
import { Container , Content, Header} from './styles';
import { FiHash } from 'react-icons/fi';
import Input from 'components/Input';
import * as Yup from 'yup';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { CommissiondayInterfacePayload }  from 'services/api/routes/commissionday/getCommissiondayById/types';
import { getCommissiondayById } from 'services/api/routes/commissionday/getCommissiondayById'
import { patchCommissiondayById } from 'services/api/routes/commissionday/registerCommissiondayById'
import Button from 'components/Button';
import NavbarStationary from 'components/NavbarStationary';

interface RouteParams {
  id: string
}

const CommissiondayEdit: React.FC = () => {

  const params = useParams<RouteParams>();
  const history = useHistory();
  const { signOut  } = useAuth();
  const [commissionday, setCommissionday] = useState<CommissiondayInterfacePayload>()
  const formRef = useRef<FormHandles>(null);

  const fetchCommissionday = async (id:number) => {
    try {
      const response = await getCommissiondayById(id)
      setCommissionday(response)
    } catch (error) {
      console.log(error)
    }
  }
  // Effect
  useEffect(() => {
    fetchCommissionday(parseInt(params.id))
  }, [])

const handleclearfile = () =>{
  setCommissionday({})
  }


const handleSubmit = useCallback(async (data: CommissiondayInterfacePayload) => {
  try {
    formRef.current?.setErrors({});

    const schema = Yup.object().shape({
      id: Yup.string().required('obrigatório'),
      day: Yup.string().required('brigatório'),
      max_commission: Yup.string().required('obrigatório'),
      min_commission: Yup.string().required('obrigatório'),
      created_at: Yup.string().required('obrigatório'),
      updated_at: Yup.string().required('obrigatório'),

    });

    await schema.validate(data, {
      abortEarly: false,
    });

    await patchCommissiondayById(+params.id, data);
    history.push('/');
  } catch (err) {
    if (err instanceof Yup.ValidationError) {
       console.log(err)
      return;
    }

  }
},
[history]);

return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={"Comissão do Dia"}></NavbarStationary>
    </Header>
    <Content>
            <Form ref={formRef} onSubmit={handleSubmit}>
                <h1></h1>
                <Input name="id" icon={FiHash} value={commissionday?.id} onFocus={()=>handleclearfile()} placeholder={"ID"}    />
                <Input name="day" icon={FiHash} value={commissionday?.days} onFocus={()=>handleclearfile()} placeholder={"Dia"} />
                <Input name="max_commission" icon={FiHash} value= {commissionday?.max_commission} onFocus={()=>handleclearfile()} placeholder={"Comissão Max"}/>
                <Input name="min_commission" icon={FiHash} value= {commissionday?.min_commission} onFocus={()=>handleclearfile()} placeholder={"Comissão Min"}/>
                <Input name="created_at" icon={FiHash} value= {commissionday?.created_at} onFocus={()=>handleclearfile()} placeholder={"Data Criação"}/>
                <Input name="updated_at" icon={FiHash} value= {commissionday?.updated_at} onFocus={()=>handleclearfile()} placeholder={"Data de Atualização"}/>
                <Button  type="submit">Salvar</Button>
              </Form>

              <Link to={'/'}><Button  type="submit">Voltar</Button></Link>
    </Content>
   
  </Container>
  
  </>
);
};

export default CommissiondayEdit;

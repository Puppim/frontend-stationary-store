import React, { useState, useEffect, useCallback, useRef } from 'react';
import {Link, useParams} from 'react-router-dom';
import { Container , Content, Header } from './styles';
import { FiUser,FiPhone,FiMail } from 'react-icons/fi';
import Input from 'components/Input';
import * as Yup from 'yup';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { CustomersInterfacePayload }  from 'services/api/routes/customers/getCustomersById/types';
import { getCustomersById } from 'services/api/routes/customers/getCustomersById'
import { patchCustomersById } from 'services/api/routes/customers/registerCustomersById'
import Button from 'components/Button';
import NavbarStationary from 'components/NavbarStationary';

interface RouteParams {
  id: string
}

const CustomersEdit: React.FC = () => {

  const params = useParams<RouteParams>();
  const history = useHistory();
  const { signOut  } = useAuth();
  const [customer, setCustomer] = useState<CustomersInterfacePayload>()
  const formRef = useRef<FormHandles>(null);

  const fetchCustomer = async (id:number) => {
    try {
      const response = await getCustomersById(id)
      setCustomer(response)
    } catch (error) {
      console.log(error)
    }
  }
  // Effect
  useEffect(() => {
    fetchCustomer(parseInt(params.id))
  }, [])

const handleclearfile = () =>{
  setCustomer({})
  }


const handleSubmit = useCallback(async (data: CustomersInterfacePayload) => {
  try {
    formRef.current?.setErrors({});

  

    const schema = Yup.object().shape({
      first_name: Yup.string().required('Nome obrigatório'),
      last_name: Yup.string().required('Sobrenome obrigatório'),
      email: Yup.string().required('E-mail obrigatório'),
      telephone: Yup.string().required('Telefone obrigatório'),

    });

    await schema.validate(data, {
      abortEarly: false,
    });

    await patchCustomersById(+params.id, data);
    // await api.patch(`api/customers/${params.id}/`, data);
    history.push('/');
  } catch (err) {
    if (err instanceof Yup.ValidationError) {
       console.log(err)
      return;
    }



    

  }
},
[history]);

return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={"Clientes Edit"}></NavbarStationary>
    </Header>
    <Content>
            <Form ref={formRef} onSubmit={handleSubmit}>
                <h1></h1>
                <Input name="first_name" icon={FiUser} value={customer?.first_name} onFocus={()=>handleclearfile()} placeholder={"Nome"}    />
                <Input name="last_name" icon={FiUser} value={customer?.last_name} onFocus={()=>handleclearfile()} placeholder={"Sobrenome"} />
                <Input name="email" icon={FiMail} value= {customer?.email} onFocus={()=>handleclearfile()} placeholder={"E-mail"}/>
                <Input name="telephone" icon={FiPhone} value={customer?.telephone}  onFocus={()=>handleclearfile()} placeholder={"Telefone"}  />
                <Button  type="submit">Salvar</Button>
              </Form>

              <Link to={'/'}><Button  type="submit">Voltar</Button></Link>
    </Content>
  
  </Container>
  
  </>
);
};

export default CustomersEdit;

import React, { useCallback, useRef, useState } from 'react';
import {
  FiArrowLeft, FiLock,
} from 'react-icons/fi';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import * as Yup from 'yup';

import PasswordChecklist from '../../components/PasswordAlert/dist';
import getValidationErrors from '../../utils/getValidationErros';
import { Link, useHistory } from 'react-router-dom';
import {useParams} from 'react-router-dom';
import api from '../../services/apiClient';
import { useToast } from '../../hooks/ToastContext';
import { Container, Content, Background } from './styles';
import Input from '../../components/Input';
import Button from '../../components/Button';
import LogoImg from 'assets/wallpaper.jpg';

interface RouteParams {
  token: string,
  uidb64: string
}


interface ResetPasswordQuery {
  password: string;
  token: string;
  uidb64: string;

}

const SignUp: React.FC = () => {

  const params = useParams<RouteParams>();
  const formRef = useRef<FormHandles>(null);
  const [password, setPassword] = useState("");
  const [passwordAgain, setPasswordAgain] = useState("");

  const { addToast } = useToast();
  const history = useHistory();
  const handleSubmit = useCallback(async (data: ResetPasswordQuery) => {
    try {
      formRef.current?.setErrors({});
      const schema = Yup.object().shape({
        password: Yup.string().min(6, 'No mínimo 6 dígitos'),
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      data.uidb64 = params.uidb64;
      data.token = params.token;
      console.log(data);
      await api.patch('api/password-reset-complete', data);

      history.push('/');

      addToast({
        type: 'success',
        title: 'Senha alterada',
        description: 'Você já pode fazer o logon no site',
      });
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errors = getValidationErrors(err);

        formRef.current?.setErrors(errors);
        return;
      }

      addToast({
        type: 'error',
        title: 'Erro no cadastro',
        description: 'Ocorreu um errro ao fazer o cadastro',
      });
    }
  }, [addToast, history]);

  return (
    <Container>
      <Background />
      <Content>
        <img src={LogoImg} alt="logo" />
        <Form ref={formRef} onSubmit={handleSubmit}>
          <h1>Nova senha</h1>

          <Input name="password" icon={FiLock} type="password" placeholder="Nova senha" onChange={e => setPassword(e.target.value)} />
          <Input name="password2" icon={FiLock} type="password" placeholder="Confirme a Senha" onChange={e => setPasswordAgain(e.target.value)}/>
          <PasswordChecklist
                rules={["length","number","match"]}
                minLength={5}
                value={password}
                valueAgain={passwordAgain}
                onChange={(isValid) => {}}
          />

          <Button type="submit">Salvar</Button>
        </Form>
        <Link to="/">
          <FiArrowLeft />Voltar para o Logon
        </Link>

      </Content>

    </Container>
  );
};

export default SignUp;

import React, { useState, useEffect, useCallback, useRef } from 'react';
import {Link, useParams} from 'react-router-dom';
import { Container , Content, Header} from './styles';
import { FiUser,FiMail } from 'react-icons/fi';
import Input from 'components/Input';
import * as Yup from 'yup';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { ProductInterfacePayload }  from 'services/api/routes/product/getProductById/types';
import { getProductById } from 'services/api/routes/product/getProductById'
import { patchProductById } from 'services/api/routes/product/registerProductById'
import Button from 'components/Button';
import NavbarStationary from 'components/NavbarStationary';

interface RouteParams {
  id: string
}

const ProductEdit: React.FC = () => {

  const params = useParams<RouteParams>();
  const history = useHistory();
  const { signOut  } = useAuth();
  const [product, setProduct] = useState<ProductInterfacePayload>()
  const formRef = useRef<FormHandles>(null);

  const fetchProduct = async (id:number) => {
    try {
      const response = await getProductById(id)
      setProduct(response)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    fetchProduct(parseInt(params.id))
  }, [])

const handleclearfile = () =>{
  setProduct({})
  }

const handleSubmit = useCallback(async (data: ProductInterfacePayload) => {
  try {
    formRef.current?.setErrors({});


    const schema = Yup.object().shape({
      description: Yup.string().required('obrigatório'),
      unitary_value: Yup.string().required('obrigatório'),
      commission: Yup.string().required('obrigatório'),
      id: Yup.string().required('obrigatório'),

    });
    await schema.validate(data, {
      abortEarly: false,
    });
    await patchProductById(+params.id, data);
    history.push('/');
  } catch (err) {
    if (err instanceof Yup.ValidationError) {
       console.log(err)
      return;
    }
  }
},
[history]);

return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={"Edit Produto"}></NavbarStationary>
    </Header>
    <Content>
            <Form ref={formRef} onSubmit={handleSubmit}>
                <h1></h1>
                <Input name="description" icon={FiUser} value={product?.description} onFocus={()=>handleclearfile()} placeholder={"Produto"}    />
                <Input name="unitary_value" icon={FiUser} value={product?.unitary_value} onFocus={()=>handleclearfile()} placeholder={"Valor"} />
                <Input name="commission" icon={FiMail} value= {product?.commission} onFocus={()=>handleclearfile()} placeholder={"Comissão"}/>
                <Input name="id" icon={FiMail} value= {product?.id} onFocus={()=>handleclearfile()} placeholder={"ID"}/>
                <Button  type="submit">Salvar</Button>
              </Form>

              <Link to={'/'}><Button  type="submit">Voltar</Button></Link>
    </Content>

  </Container>
  
  </>
);
};

export default ProductEdit;

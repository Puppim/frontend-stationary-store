import React, { useState, useEffect } from 'react';
import { Container , Content, Header, ContentButton, Itenslist} from './styles';
import { Link, useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { getSale }  from 'services/api/routes/sale/getSale';
import { SaleInterface }  from 'services/api/routes/sale/getSale/types';
import ListSale  from 'components/ListItensSale';

import Button from "components/Button";
import NavbarStationary from 'components/NavbarStationary';

const Sale: React.FC = () => {

  const [sale, setSale] = useState<SaleInterface[]>([])

  const fetchSale = async () => {
    try {
      const response = await getSale()
      setSale(response.results)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    fetchSale()
  }, [])

return(
  <>
  <Container>
    <Header>
      <NavbarStationary title={"Vendas"}></NavbarStationary>
    </Header>
    <Content>
      <ContentButton>
      <Link to="newsale">
      <Button>Inserir nova Venda</Button>
      </Link>
      </ContentButton>
    <Itenslist>
       <ListSale sale={sale} ></ListSale>
    </Itenslist>
    </Content>
  </Container>
  
  </>
);
};

export default Sale;

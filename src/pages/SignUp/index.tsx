import React, { useCallback, useRef , useState} from 'react';

import PasswordChecklist from '../../components/PasswordAlert/dist';


import {  FiMail, FiLock, FiUser,FiHash, FiPhone, FiCalendar} from 'react-icons/fi';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import * as Yup from 'yup';
import { Link, useHistory } from 'react-router-dom';
import getValidationErrors from '../../utils/getValidationErros';
import CPFcheck from '../../components/CPFCheck'

import api from '../../services/apiClient';

import { useToast } from '../../hooks/ToastContext';

import { Container, Content } from './styles';

import Input from '../../components/Input';
import Button from '../../components/Button';

import LinkStyled from '../../components/LinkStyled/LinkStyled';

import logoStationary from 'assets/coloredpencils.png';

import InputMask from '../../components/InputMask';
import Modal from '../../components/Modal';
import TermsUser from '../../components/TermsUser';


interface SignUpFormData {
  username: string;
  email: string;
  data_nasc?: string;
  password: string;
  cpf: string;
  telefone: string;
  sobrenome: string;
}


const SignUp: React.FC = () => {

  const [isModalVisible, setIsModalVisible] = useState(false);
  const [ischecked, setIschecked] = useState(true);
  const [password, setPassword] = useState("");
  const [passwordAgain, setPasswordAgain] = useState("");
  const formRef = useRef<FormHandles>(null);
  const { addToast } = useToast();
  const history = useHistory();

  const activeCheckbox = ()=> { 
    setIschecked(!ischecked);
    console.log(ischecked)
  }

  const activeModal = ()=> { 
    setIsModalVisible(!isModalVisible);
  }

  const handleSubmit = useCallback(async (data: SignUpFormData) => {
    try {
      formRef.current?.setErrors({});
      const schema = Yup.object().shape({

        username: Yup.string().required('Nome obrigatório'),
        sobrenome: Yup.string().required('Sobrenome obrigatório'),
        email: Yup.string().required('Email obrigatório').email('Email inválido'),
        data_nasc: Yup.string().transform(value => value.replace(/[\_]/g,'')).min(10, 'Data de Nascimento obrigatório').required('Data de Nascimento obrigatório'),
        telefone: Yup.string().transform(value => value.replace(/[\_]/g,'')).min(15, 'Telefone obrigatório').required('Telefone obrigatório'),
        cpf: Yup.string().transform( value => value.replace(/[\_]/g,'')).transform(
            value => {
              let test= CPFcheck.cpf(data.cpf)
              if(!test){
                return ''
              }
              else{
                return value
              }

            }
        ).min(14, 'CPF invadi').required('CPF inválido.'),
        password: Yup.string().min(6, 'No mínimo 6 dígitos'),
      });

      await schema.validate(data, {
        abortEarly: false,
      });
                

      const dataStationaryRest ={
        "username": data.email,
        "email": data.email,
        "nome" : data.username,
        "data_nasc" : data.data_nasc,
        "cpf" : data.cpf,
        "password": data.password,
        "telefone": data.telefone,
        "sobrenome": data.sobrenome
      }

      await api.post('/api/registeraddcliente/', dataStationaryRest);       

      history.push('/');

      addToast({
        type: 'success',
        title: 'Cadastro realizado',
        description: 'Você já pode fazer o logon no site',
      });
    } catch (err) {
      if (err instanceof Yup.ValidationError) {
        const errors = getValidationErrors(err);

        formRef.current?.setErrors(errors);
        return;
      }

      addToast({
        type: 'error',
        title: 'Erro no cadastro',
        description: 'Ocorreu um errro ao fazer o cadastro',
      });
    }
  }, [addToast, history]);

  return (
    <Container>
          {isModalVisible ? 

        <Modal buttonName="Fechar" width='100%' height='100%' className="teste" onClose={()=>setIsModalVisible(false)}  >
            <h1 id="Termos" >TERMO DE USO</h1>
            <div className="contentTerms" >
              <div className="conteudo">
          
                  <TermsUser></TermsUser>
            </div>
        </div>
        </Modal>

      : null}

      <Content>
        <div className="formuser">
        <Form ref={formRef} onSubmit={handleSubmit}>
          <h1>Faça seu cadastro</h1>
          <Input name="username" icon={FiUser} placeholder="Nome" />
          <Input name="sobrenome" icon={FiUser} placeholder="Sobrenome" />
          <InputMask name="data_nasc" icon={FiCalendar} placeholder={"Data de nascimento"} mask="99/99/9999"/>
          <Input name="email" icon={FiMail} placeholder="E-mail" />
          <InputMask name="telefone" icon={FiPhone}   placeholder={"Telefone"} mask="(99) 99999-9999" />
          <InputMask  icon={FiHash}  name="cpf"  placeholder={"CPF"}  mask="999.999.999-99"></InputMask>
          <Input name="password" icon={FiLock} type="password" placeholder="Senha" onChange={e => setPassword(e.target.value)} />
          <Input name="password2" icon={FiLock} type="password" placeholder="Confirme a Senha" onChange={e => setPasswordAgain(e.target.value)}/>
          <PasswordChecklist
                rules={["length","number","match"]}
                minLength={5}
                value={password}
                valueAgain={passwordAgain}
                onChange={(isValid) => {}}
          />
          <div className="formchecked">
            <input type="checkbox" id="scales" name="scales" onClick={()=>{activeCheckbox()}}/> 
            <div className="textChecked"> Aceito os <a href="#Termos" onClick={()=>{activeModal()}}>termos</a>  de uso </div>
          </div>
        
          <Button disabled={ischecked} type="submit">Cadastrar</Button>
        </Form>


        </div>

      </Content>

        <div className="backframe">
        <div className="back">
            <LinkStyled to="/">
                <Button>Voltar</Button>
            </LinkStyled>
        </div>
      </div>

    </Container>
  );
};

export default SignUp;

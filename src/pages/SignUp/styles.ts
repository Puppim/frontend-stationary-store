import styled from 'styled-components';
import { shade } from 'polished';
// import sigInBackgroundImg from '../../assets/sign-in-background.png';
import ImgSignUp from '../../assets/wallpaper.jpg';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  align-items: stretch;
  flex-direction: column;

  .contentTerms{
    overflow-x: auto;
  }

  .contentTerms{
    overflow-x: auto;
  }

  .navbar {
    background: #F4F0EF;
    img{
      margin-left:30px;
    }
  }


  .backframe{
    display:flex;
    flex:1;
    margin-bottom: 50px;
    justify-content:center;
  }


`;
export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 50px;
  /* place-content: center; */



.formchecked{
  display: flex;
  flex-direction: row;
  align-items: center;

  a{
    margin-left: 5px;
    margin-right: 5px;
  }
  input{
    margin-right: 5px;
  }
}

  .formuser{
    display: flex;
    width: 50%;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background: #F4F0EF;
    border: 1px solid #F4F0EF;
    box-sizing: border-box;
    border-radius: 8px;
  }

  button:disabled {
    background: ${shade(0.2, '#F4F0EF')}
  }

  @media only screen and (max-width: 700px) {
    .formuser{
    width: 100%;
    }
  }

  form {
    margin: 80px 0;
    width: 350px;
    text-align: center;

    h1 {
      margin-bottom: 24px;
    }



    a {
      color: #34C48F;
      /* display: block; */
      /* margin-top: 24px; */
      text-decoration: none;
      transition: color 0.2s;

      &:hover {
        color: ${shade(0.2, '#34C48F')}
      }
    }


  }
  > a {
    color: #fff;
      /* display: block; */
      /* margin-top: 24px; */
      text-decoration: none;
      transition: color 0.2s;

      svg {
        margin-right: 16px;
      }

      &:hover {
        color: ${shade(0.2, '#fff')}
      }
    }


`;

export const Background = styled.div`

flex:1;
background: url(${ImgSignUp}) no-repeat center;
background-size: cover;

`;

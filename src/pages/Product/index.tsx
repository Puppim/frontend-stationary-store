import React, { useState, useEffect } from 'react';

import { Container , Content, Header, Itenslist} from './styles';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { getProduct }  from 'services/api/routes/product/getProduct';
import { ProductInterface }  from 'services/api/routes/product/getProduct/types';
import ListProduct  from 'components/ListItensProduct';
import NavbarStationary from 'components/NavbarStationary';

const Product: React.FC = () => {


  const history = useHistory();
  const { signOut  } = useAuth();
  const [product, setProduct] = useState<ProductInterface[]>([])

  const fetchProduct = async () => {
    try {
      const response = await getProduct()
      setProduct(response.results)
    } catch (error) {
      console.log(error)
    }
  }
  // Effect
  useEffect(() => {
    fetchProduct()
  }, [])

return(
  <>
  <Container>

    <Header>
    <NavbarStationary title={"Produtos"}></NavbarStationary>
    </Header>
 
    <Content>
    {product.map(e=>
    <Itenslist>
       <ListProduct product={e} ></ListProduct>
    </Itenslist>
    )}
    </Content>
  
    

  </Container>
  
  </>
);
};

export default Product;

import React, { useState, useEffect, useCallback, useRef } from 'react';
import {Link, useParams} from 'react-router-dom';
import { Container , Content, Header} from './styles';
import { FiHash} from 'react-icons/fi';
import Input from 'components/Input';
import * as Yup from 'yup';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { SellerInterfacePayload }  from 'services/api/routes/seller/getSellerById/types';
import { getSellerById } from 'services/api/routes/seller/getSellerById'
import { patchSellerById } from 'services/api/routes/seller/registerSellerById'
import Button from 'components/Button';
import NavbarStationary from 'components/NavbarStationary';

interface RouteParams {
  id: string
}

const SellerEdit: React.FC = () => {

  const params = useParams<RouteParams>();
  const history = useHistory();
  const { signOut  } = useAuth();
  const [seller, setSeller] = useState<SellerInterfacePayload>()
  const formRef = useRef<FormHandles>(null);

  const fetchSeller = async (id:number) => {
    try {
      const response = await getSellerById(id)
      setSeller(response)
    } catch (error) {
      console.log(error)
    }
  }
  // Effect
  useEffect(() => {
    fetchSeller(parseInt(params.id))
  }, [])

const handleclearfile = () =>{
  setSeller({})
  }


const handleSubmit = useCallback(async (data: SellerInterfacePayload) => {
  try {
    formRef.current?.setErrors({});

    const schema = Yup.object().shape({
      first_name: Yup.string().required('Nome obrigatório'),
      last_name: Yup.string().required('Sobrenome obrigatório'),
      email: Yup.string().required('E-mail obrigatório'),
      telephone: Yup.string().required('Telefone obrigatório'),
    });

    await schema.validate(data, {
      abortEarly: false,
    });

    await patchSellerById(+params.id, data);
    history.push('/');
  } catch (err) {
    if (err instanceof Yup.ValidationError) {
       console.log(err)
      return;
    }

  }
},
[history]);

return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={"Vendedores"}></NavbarStationary>
    </Header>
    <Content>
            <Form ref={formRef} onSubmit={handleSubmit}>
                <h1></h1>
                <Input name="first_name" icon={FiHash} value={seller?.first_name} onFocus={()=>handleclearfile()} placeholder={"Nome"}    />
                <Input name="last_name" icon={FiHash} value={seller?.last_name} onFocus={()=>handleclearfile()} placeholder={"Sobrenome"} />
                <Input name="email" icon={FiHash} value= {seller?.email} onFocus={()=>handleclearfile()} placeholder={"Email"}/>
                <Input name="telephone" icon={FiHash} value= {seller?.telephone} onFocus={()=>handleclearfile()} placeholder={"Telefone"}/>
                <Input name="id" icon={FiHash} value={seller?.id}  onFocus={()=>handleclearfile()} placeholder={"ID"}  />
                <Button  type="submit">Salvar</Button>
              </Form>

              <Link to={'/'}><Button  type="submit">Voltar</Button></Link>
    </Content>
 
  </Container>
  
  </>
);
};

export default SellerEdit;

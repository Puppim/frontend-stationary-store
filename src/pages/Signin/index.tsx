import React,{useEffect, useState} from 'react';
import { FiLogIn } from 'react-icons/fi';
import { Link } from 'react-router-dom';
import { Container, Content, Background } from './styles';

import LoginCard from 'components/LoginCard';
const SignIn: React.FC = () => {



 
  return (
    <Container>
      <Content>
        <div className="blockaside">
        <aside className="asideA aside-1"> 
          <LoginCard>
          </LoginCard>
        </aside>
        
         </div>

      
        <Link to="signup">
          <FiLogIn />Criar conta
        </Link>
      </Content>
      <Background />
    </Container>

  );
};

export default SignIn;

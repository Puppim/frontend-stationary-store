import styled from 'styled-components';
import { shade } from 'polished';
import sigInBackgroundImg from '../../assets/wallpaper.jpg';

export const Container = styled.div`
  height: 100vh;
  display: flex;
  align-items: stretch;
  flex-direction: column
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap 
  width: 80%;
  height: 60%;
`;



export const Itenslist = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 20px;

`;


export const Header = styled.div`
  display: flex;
  flex-direction: row;
  background-color: red;
  width: 100%;
  height: auto;
`;

export const Footer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  height: 20%;
`;






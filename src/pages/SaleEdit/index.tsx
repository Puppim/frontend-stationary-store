import React, { useState, useEffect, useCallback, useRef } from 'react';
import {Link, useParams} from 'react-router-dom';
import { Container , Content, Header} from './styles';
import { FiHash} from 'react-icons/fi';
import Input from 'components/Input';
import * as Yup from 'yup';
import { Form } from '@unform/web';
import { FormHandles } from '@unform/core';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { SaleInterfacePayload }  from 'services/api/routes/sale/getSaleById/types';
import { getSaleById } from 'services/api/routes/sale/getSaleById'
import { patchSaleById } from 'services/api/routes/sale/registerSaleById'
import Button from 'components/Button';
import NavbarStationary from 'components/NavbarStationary';

interface RouteParams {
  id: string
}

const SaleEdit: React.FC = () => {

  const params = useParams<RouteParams>();
  const history = useHistory();
  const { signOut  } = useAuth();
  const [sale, setSale] = useState<SaleInterfacePayload>()
  const formRef = useRef<FormHandles>(null);

  const fetchSale = async (id:number) => {
    try {
      const response = await getSaleById(id)
      setSale(response)
    } catch (error) {
      console.log(error)
    }
  }
  useEffect(() => {
    fetchSale(parseInt(params.id))
  }, [])

const handleSubmit = useCallback(async (data: SaleInterfacePayload) => {
  try {
    formRef.current?.setErrors({});

    const schema = Yup.object().shape({
      number_nfe: Yup.string().required('obrigatório'),
      customers: Yup.string().required('brigatório'),
      commission_day: Yup.string().required('obrigatório'),
      seller: Yup.string().required('obrigatório'),
    });

    await schema.validate(data, {
      abortEarly: false,
    });

    await patchSaleById(+params.id, data);
    history.push('/');
  } catch (err) {
    if (err instanceof Yup.ValidationError) {
       console.log(err)
      return;
    }

  }
},
[history]);

return(
  <>
  <Container>
    <Header>
    <NavbarStationary title={`Venda ${params.id}`}></NavbarStationary>
    </Header>
    <Content>
            <Form ref={formRef} onSubmit={handleSubmit}>
                <h1></h1>
                <Input name="number_nfe" icon={FiHash} value={sale?.number_nfe} placeholder={"Nfe"}    />
                <Input name="customers" icon={FiHash} value={sale?.customers} placeholder={"Cliente ID"} />
                <Input name="commission_day" icon={FiHash} value= {sale?.commission_day} placeholder={"Comissão do Dia ID"}/>
                <Input name="seller" icon={FiHash} value={sale?.seller}  placeholder={"Vendedor ID"}  />
                <Button  type="submit">Salvar</Button>
              </Form>

              <Link to={'/'}><Button  type="submit">Voltar</Button></Link>
    </Content>
  </Container>
  
  </>
);
};

export default SaleEdit;

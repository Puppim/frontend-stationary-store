import React, { useState, useEffect } from 'react';

import { Container , Content, Header, Footer, Itenslist} from './styles';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/AuthContext';
import { getCustomers }  from 'services/api/routes/customers/getCustomers';
import { CustomersInterface }  from 'services/api/routes/customers/getCustomers/types';
import ListCustomers  from 'components/ListItensCustomers';
import NavbarStationary from 'components/NavbarStationary';

const Customers: React.FC = () => {


  const [customer, setCustomer] = useState<CustomersInterface[]>([])

  const fetchCustomer = async () => {
    try {
      const response = await getCustomers()
      setCustomer(response.results)
    } catch (error) {
      console.log(error)
    }
  }
  // Effect
  useEffect(() => {
    fetchCustomer()
  }, [])

return(
  <>
  <Container>

    <Header>
    <NavbarStationary title={"Clientes"}></NavbarStationary>
    </Header>
 
    <Content>
    {customer.map(e=>
    <Itenslist>
       <ListCustomers customer={e} ></ListCustomers>
    </Itenslist>
    )}
    </Content>
  
  
  </Container>
  
  </>
);
};

export default Customers;
